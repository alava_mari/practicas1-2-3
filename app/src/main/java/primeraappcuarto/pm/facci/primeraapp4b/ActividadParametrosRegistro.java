package primeraappcuarto.pm.facci.primeraapp4b;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActividadParametrosRegistro extends AppCompatActivity {
    TextView Nombres,Apellidos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_parametros_registro);
        Nombres = (TextView)findViewById(R.id.lblNombre);
        Apellidos = (TextView)findViewById(R.id.lblApellido);
        Bundle bundle = this.getIntent().getExtras();
        Nombres.setText("Nombres: "+bundle.getString("nombres"));
        Apellidos.setText("Apellidos: "+bundle.getString("apellidos"));
    }
}
