package primeraappcuarto.pm.facci.primeraapp4b;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActividadRegistrar extends AppCompatActivity {
    EditText Nombres,Apellidos;
    Button Enviar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_registrar);
        Nombres = (EditText)findViewById(R.id.txtNombre);
        Apellidos = (EditText)findViewById(R.id.txtApellido);
        Enviar = (Button)findViewById(R.id.btnEnviar);

        Enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ActividadRegistrar.this,ActividadParametrosRegistro.class);
                Bundle bundle = new Bundle();
                // el método put fija los parámetro a enviar mediante un id
                bundle.putString("nombres",Nombres.getText().toString());
                bundle.putString("apellidos",Apellidos.getText().toString());
                // método putExtras envia un objeto de tipo bundle como un sólo parámetro entre actividades
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
